
import base64
import hashlib
import hmac
import time
import json

def encode(data, secret):
    data_b64 = base64.b64encode(data.encode('utf-8'))
    sig = hmac.new(secret.encode('utf-8'), data_b64, hashlib.sha256)
    return (data_b64, sig.hexdigest())
